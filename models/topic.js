const {Schema, model} = require('mongoose');
const Message  = require('./message');

const TopicSchema = Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    field: {
        type: String,
        required: true
    },
    description: String,
    participants: {
        type: Array,
        required: false,
        unique: false
    },
    owner: {
        type: String,
        required: true,
        unique: false
    }
})

//Define the model
const Topic = model('Topic', TopicSchema);

//Methods to manipulate data from database
Topic.addTopic = (topic, callback) => {
    topic.save(callback);
}

Topic.getTopicByName = (topicName, callback) => {
    Topic.find({name: topicName}, (err, topic) => {
        if(err) {
            return callback({msg: "There was an error on getting the topic with name -> " + topicName + " : " + err});
        } else {
            return callback(null, topic);
        }
    })
}

Topic.getTopicChatRoom = (topicName, callback) => {
    Message.getMessagesByTopic(topicName, (err, messages) => {
        if (err) {
            return callback({msg: err});
        } else {
            return callback(null, messages);
        }
    })
}

Topic.getTopics = () => {
    return Topic.find({}).lean();
}

//Export this module
module.exports = Topic