const {Schema, model} = require('mongoose')

const MessageSchema = Schema({
    created: {
        type: Date,
        require: true,
    },
    from: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    topicName: {
        type: String,
        required: true
    }
})

//Define the model
const Message = model('Message', MessageSchema);

//Methods to manipulate data from database
Message.addMessage = (message, callback) => {
    message.save(callback);
}

Message.getMessages = () => {
    return Message.find({}).lean();
}

Message.getMessagesByTopic = (name, callback) => {
    Message.find({topicName: name}, (err, messages) => {
        if (err){
            return callback({msg: "There was an error on getting topic's messages"})
        }else {
            return callback(null, messages);
        }
    });
}

//Export this module
module.exports = Message;