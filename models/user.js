const {Schema, model} = require('mongoose');
const bcrypt = require('bcryptjs');

//Define user schema
const UserSchema = Schema({
    username: {
        type: String,
        unique: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    birthdate: String
});

//Define the model
const User = model('User', UserSchema);

//Methods to manipulate data from database
User.getUserById = function(id, callback) {
    User.findById(id, callback).lean();
}

User.getUserByUsername = function(username, callback) {
    let query = {username: username};
    User.findOne(query, callback);
}

User.getUsers = () => {
    return User.find({}, '-password').lean();
}

User.addNewUser = function(newUser, callback) {
    User.getUserByUsername(newUser.username, (err, user) => {
        if (err)
            return callback({msg: "There was an error on getting the user"});
        if (user) {
            return callback({msg: "Username is already in use"});
        } else {
            //Password hashing initialization
            //A cryptographic salt is made up of random bits added to each password instance before its hashing
            //Rounds define math.power(2, rounds) loops of hashing
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err)
                        return callback({msg: "There was an error on adding the new user"});
                    newUser.password = hash;
                    newUser.save(callback);
                });
            });
        }
    });
};

User.authenticate = function(username, password, callback) {
    User.getUserByUsername(username, (err, user) => {
        if (err)
            return callback({msg: "There was an error on getting the user"});
        if (!user) {
            let error = {msg: "Wrong username"};
            return callback(error);
        } else {
            bcrypt.compare(password, user.password, (err, result) => {
                if (result === true) {
                    return callback(null, user);
                } else {
                    let error = {msg: "Wrong password"};
                    return callback(error);
                }
            });
        }
    });
};

//Export this module
module.exports = User