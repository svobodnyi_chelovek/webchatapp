const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const logger = require('../log/logger').logger;
const User = require('../models/user');

module.exports = passport => {
    let options = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Basic'),
        secretOrKey: 'privateKey'
    };

    passport.use(
        new JwtStrategy(options, (jwt_payload, done) => {
            User.getUserByUsername(jwt_payload.username, (err, user) => {
                if (err){
                    logger.error(err);
                    return done(err, null);
                }
                if (user) {
                    let signData = {
                        id: user._id,
                        username: user.username
                    };
                    return done(null, signData);
                } else {
                    return done(null, false, jwt_payload);
                }
            });
        })
    );
}