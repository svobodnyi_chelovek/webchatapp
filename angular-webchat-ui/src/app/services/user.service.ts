import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserModel } from '../models/user.model';

// Service to handle API calls to the server regarding user authentication
@Injectable()
export class User {
  readonly apiURL = 'http://localhost:3000';
  private authToken = '';
  private userData: UserModel = {
    username: '',
    email: '',
    birthdate: ''
  };

  constructor(
    private http: HttpClient
  ) {}

  // User registration
  createNewUser(userdata: string): any {
    const apiRegisterUrl = this.apiURL + '/create';
    return this.http.post(apiRegisterUrl, userdata);
  }

  // User authentication
  authenticateUser(userdata: string): any {
    const apiLoginUrl = this.apiURL + '/login';
    return this.http.post(apiLoginUrl, userdata);
  }

  // Store authentication data to localStorage
  storeUserData(token: string, user: UserModel): void {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
  }

  // Get user data info
  getUserData(): UserModel {
    this.userData = JSON.parse(localStorage.getItem('user') as string);
    return this.userData;
  }

  // Get token from local storage
  getToken(): string {
    return localStorage.getItem('token') as string;
  }

  // Check if the user token is not expired
  loggedIn(): boolean {
    const jwtHelper = new JwtHelperService();
    return !(jwtHelper.isTokenExpired(this.getToken()));
  }

  // Logout the user
  logout(): void {
    this.authToken = '';
    this.userData = {
      username: '',
      email: '',
      birthdate: ''
    };
    localStorage.clear();
  }
}
