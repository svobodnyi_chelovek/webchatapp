import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { User } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(public user: User, public router: Router) {}

  canActivate(): boolean {
    if (this.user.loggedIn()){
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
