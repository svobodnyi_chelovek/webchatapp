import {Injectable} from '@angular/core';
import {Message} from '../models/message.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Topic} from '../models/topic.model';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  readonly apiURL = 'http://localhost:3000';

  private initTopic: Topic = {
    name: 'No selected topic',
    field: '',
    description: '',
    participants: [],
    owner: ''
  };
  private topic = new BehaviorSubject<Topic>(this.initTopic);
  selectedTopic: Observable<Topic> = this.topic.asObservable();

  constructor(
    private http: HttpClient,
    public user: User
  ) {}

  // Topic creation
  createTopic(topicData: string): any {
    const apiCreateTopicUrl = this.apiURL + '/createTopic/' + this.user.getUserData().username;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + this.user.getToken()
      })
    };
    return this.http.post(apiCreateTopicUrl, topicData, httpOptions);
  }

  // Get topic list
  getTopics(): any {
    const apiTopicsUrl = this.apiURL + '/topics';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + this.user.getToken()
      })
    };
    return this.http.get<Topic[]>(apiTopicsUrl, httpOptions);
  }

  // Get topic data by its name
  getTopic(topicName: string): any {
    const apiTopicUrl = this.apiURL + '/topic/' + topicName;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + this.user.getToken()
      })
    };
    return this.http.get<Topic>(apiTopicUrl, httpOptions);
  }

  // Get topic chat
  getTopicConversation(topicName: string): any {
    const apiConversationUrl = this.apiURL + '/conversation/' + topicName;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + this.user.getToken()
      })
    };
    return this.http.get<Message[]>(apiConversationUrl, httpOptions);
  }

  // Load topic data to localStorage to keep it after refresh
  loadTopicData(): void {
    localStorage.setItem('topic', JSON.stringify(this.topic.value));
  }

  // Get active topic name from localStorage
  getTopicData(): Topic {
    return JSON.parse(localStorage.getItem('topic') as string);
  }

  // Select the topic for chat
  selectTopic(topic: Topic): void {
    this.topic.next(topic);
  }

  // Check if the user participate in the active topic chat session
  isParticipant(topic: Topic): boolean {
    return topic.participants.includes(this.user.getUserData().username);
  }

  // Update number of participants
  addTopicParticipant(): any {
    this.topic.value.participants.push(this.user.getUserData().username);
    const addParticipantUrl = this.apiURL + '/addParticipant/' + this.topic.value.name;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + this.user.getToken()
      })
    };
    return this.http.put(addParticipantUrl, this.user.getUserData(), httpOptions);
  }

  excludeTopicParticipant(): any {
    this.topic.value.participants.forEach((element, index) => {
      if (element === this.user.getUserData().username) { delete this.topic.value.participants[index]; }
    });
    const excludeParticipantUrl = this.apiURL + '/excludeParticipant/' + this.topic.value.name;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + this.user.getToken()
      })
    };
    return this.http.put(excludeParticipantUrl, this.user.getUserData(), httpOptions);
  }
}
