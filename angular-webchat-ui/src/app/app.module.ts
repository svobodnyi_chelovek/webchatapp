import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {FlashMessagesModule} from 'flash-messages-angular';
import {AppRoutingModule} from './app-routing.module';
import {SocketIoConfig, SocketIoModule} from 'ngx-socket-io';
import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {HomeComponent} from './components/home/home.component';
import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';
import {ChatRoomComponent} from './components/chat-room/chat-room.component';
import {ProfileComponent} from './components/profile/profile.component';
import {CreateComponent} from './components/create/create.component';
import {TopicListComponent} from './components/topic-list/topic-list.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {PageNotFoundComponentComponent} from './components/page-not-found-component/page-not-found-component.component';
import {User} from './services/user.service';
import {UserComponent} from './components/user/user.component';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';

const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    ChatRoomComponent,
    ProfileComponent,
    TopicListComponent,
    SidebarComponent,
    PageNotFoundComponentComponent,
    UserComponent,
    CreateComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FlashMessagesModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    SocketIoModule.forRoot(config),
    LoggerModule.forRoot({
      serverLoggingUrl: 'localhost:3000/logs',
      level: NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.ERROR
    })
  ],
  providers: [
    User
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
