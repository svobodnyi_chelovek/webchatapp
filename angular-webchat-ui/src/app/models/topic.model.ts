export interface Topic {
  name: string;
  field: string;
  description: string;
  participants: Array<string>;
  owner: string;
}
