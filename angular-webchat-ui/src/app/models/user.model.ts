export interface UserModel {
  username: string;
  email: string;
  birthdate: string;
}
