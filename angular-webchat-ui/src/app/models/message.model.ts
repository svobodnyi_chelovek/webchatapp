export interface Message {
  created: number;
  from: string;
  text: string;
  topicName: string;
}
