import { Component, OnInit } from '@angular/core';
import { User } from '../../services/user.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {
  username = '';

  constructor(
    public user: User,
    public router: Router,
    public flashMessageService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    this.username = this.user.getUserData().username;
  }

  // Log out the user
  onLogoutClick(): void {
    this.user.logout();
    location.reload();
    this.flashMessageService.show('Successful log out', {cssClass: 'alert-popup alert-success', timeout: 2500});
  }

}
