import { Component, OnInit } from '@angular/core';
import { User } from '../../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  username = '';
  email = '';
  birthdate = '';

  constructor(public user: User) {}

  ngOnInit(): void {
    this.username = this.user.getUserData().username;
    this.email = this.user.getUserData().email;
    this.birthdate = this.user.getUserData().birthdate;
  }
}
