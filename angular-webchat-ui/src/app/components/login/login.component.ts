import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../services/user.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';
import { UserModel } from 'src/app/models/user.model';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
  providers: [User]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    public user: User,
    public router: Router,
    public flashMessageService: FlashMessagesService,
    public logger: NGXLogger
  ) {
    this.loginForm = new FormGroup({});
  }

  ngOnInit(): void {
    this.checkLoggedIn();

    this.loginForm = new FormGroup({
      username: new FormControl(this.username, [
        Validators.required
      ]),
      password: new FormControl(this.password, [
        Validators.required
      ])
    });
  }

  get username(): any {
    return this.loginForm.get('username');
  }

  get password(): any {
    return this.loginForm.get('password');
  }

  // Check if the user is already logged in
  checkLoggedIn(): void {
    if (this.user.loggedIn()) {
      // Navigate to the corresponding page
      this.router.navigate(['/user']);
    }
  }

  // Check user credentials
  onLoginSubmit(): void {
    if (this.loginForm?.invalid) {
      this.flashMessageService.show('Invalid form', {cssClass: 'alert-popup alert-danger', timeout: 2000});
    } else {
      // tslint:disable-next-line:max-line-length
      this.user.authenticateUser(this.loginForm.value).subscribe((data: { success: boolean; token: string; user: UserModel; msg: string | undefined; }) => {
        if (data.success) {
          // Store user data
          this.user.storeUserData(data.token, data.user);
          // Navigate to corresponding page
          this.router.navigate(['/user']);
          this.flashMessageService.show('Successful login', { cssClass: 'alert-popup alert-success', timeout: 2000 });
        } else {
          this.flashMessageService.show(data.msg, {cssClass: 'alert-popup alert-danger', timeout: 2500});
          this.logger.error(data.msg);
        }
      });
    }
  }
}
