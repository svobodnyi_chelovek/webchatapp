import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FlashMessagesService } from 'flash-messages-angular';
import { User } from '../../services/user.service';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass'],
  providers: [ User ]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(
    public user: User,
    public router: Router,
    public flashMessageService: FlashMessagesService,
    public logger: NGXLogger
  ) {
    this.registerForm = new FormGroup({});
  }

  ngOnInit(): void {
    this.checkLoggedIn();

    this.registerForm = new FormGroup({
      username: new FormControl(this.username, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(25)
      ]),
      email: new FormControl(this.email, [
        Validators.required,
        Validators.email
      ]),
      birthdate: new FormControl(this.birthdate, [
        Validators.required
      ]),
      password: new FormControl(this.password, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(24)
      ]),
      confirmPassword: new FormControl(this.confirmPassword, [
        Validators.required
      ])
    });
  }

  get username(): any { return this.registerForm.get('username'); }
  get email(): any { return this.registerForm.get('email'); }
  get birthdate(): any { return this.registerForm.get('birthdate'); }
  get password(): any { return this.registerForm.get('password'); }
  get confirmPassword(): any { return this.registerForm.get('confirmPassword'); }

  // Check if the user is already logged in
  checkLoggedIn(): void {
    if (this.user.loggedIn()) {
      this.router.navigate(['/user']);
    }
  }

  // Create a new user
  onRegisterSubmit(): any {
    if (this.password?.value !== this.confirmPassword?.value) {
      this.flashMessageService.show('Password and Confirm Password must match', { cssClass: 'alert-popup alert-danger', timeout: 2000 });
    } else if (this.registerForm?.invalid){
      this.flashMessageService.show('Invalid form', { cssClass: 'alert-popup alert-danger', timeout: 2000 });
    }else {
      this.user.createNewUser(this.registerForm.value).subscribe((data: { success: boolean; msg: string | undefined; }) => {
        if (data.success){
          this.router.navigate(['/login']);
          this.flashMessageService.show('Account has been created successfully', { cssClass: 'alert-popup alert-success', timeout: 2000 });
        } else {
          this.registerForm.reset();
          this.flashMessageService.show(data.msg, { cssClass: 'alert-popup alert-danger', timeout: 3000 });
          this.logger.error(data.msg);
        }
      });
    }
  }

}
