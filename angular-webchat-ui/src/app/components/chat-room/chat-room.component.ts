import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Message } from '../../models/message.model';
import { User } from '../../services/user.service';
import { ChatService } from '../../services/chat.service';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.sass']
})
export class ChatRoomComponent implements OnInit, OnDestroy {
  topicName: string;
  description = '';
  sendForm: FormGroup;
  messageList: Message[] = [];

  constructor(
    public user: User,
    private chatService: ChatService,
    private socket: Socket
  ) {
    this.topicName = 'No topic selected';
    this.sendForm = new FormGroup({});
  }

  ngOnInit(): void {
    // Initialize send message form
    this.sendForm = new FormGroup({
      messageContent: new FormControl(this.messageContent, [
        Validators.required
      ])
    });

    // Get topic data
    this.topicName = this.chatService.getTopicData()?.name;
    this.description = this.chatService.getTopicData()?.description;

    // Get topic messages
    this.chatService.getTopicConversation(this.topicName).subscribe((data: { success: boolean; messages: Message[]; }) => {
      if (data.success) {
        this.messageList = data.messages;
      }
    });

    this.setupSocketConnection();
  }

  ngOnDestroy(): void {
    this.chatService.excludeTopicParticipant();
  }

  get messageContent(): any { return this.sendForm.get('messageContent'); }

  setupSocketConnection(): void {
    this.socket.on('message', (data: { room: string; msg: Message; }) => {
      if (data.room === this.topicName){
        this.messageList.push(data.msg);
      }
    });
  }

  onSendMessage(): void {
    const newMessage: Message = {
      created: Date.now(),
      from: this.user.getUserData().username,
      text: this.sendForm.value.messageContent,
      topicName: this.topicName
    };

    // SEND message to server
    this.socket.emit('message', { msg: newMessage, room: this.topicName });
    // Update current user chat box
    this.messageList.push(newMessage);
    this.sendForm.setValue({ messageContent: ''} );
  }
}
