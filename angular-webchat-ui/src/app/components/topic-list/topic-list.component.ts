import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Topic } from '../../models/topic.model';
import { User } from '../../services/user.service';
import { ChatService } from '../../services/chat.service';

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.sass']
})
export class TopicListComponent implements OnInit {
  topicList: Topic[] = [];

  constructor(
    public user: User,
    public router: Router,
    public chatService: ChatService
  ) {}

  ngOnInit(): void {
    this.chatService.getTopics().subscribe((data: { success: boolean; topics: Topic[]; }) => {
      if (data.success) {
        this.topicList = data.topics;
      }
    });
  }

  getTopicChat(topic: Topic): any {
    if (!(this.chatService.isParticipant(topic))) {
      this.chatService.excludeTopicParticipant().subscribe();
      this.chatService.selectTopic(topic);
      this.chatService.addTopicParticipant().subscribe();
      this.chatService.loadTopicData();
    } else {
      this.chatService.selectTopic(topic);
      this.chatService.loadTopicData();
    }
    this.router.navigate(['user/chat']);
  }
}
