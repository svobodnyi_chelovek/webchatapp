import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FlashMessagesService } from 'flash-messages-angular';
import { Router } from '@angular/router';
import { User } from '../../services/user.service';
import {ChatService} from '../../services/chat.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {
  createForm: FormGroup;

  constructor(
    public router: Router,
    public user: User,
    public chatService: ChatService,
    public flashMessageService: FlashMessagesService
  ) {
    this.createForm = new FormGroup({});
  }

  ngOnInit(): void {
    this.createForm = new FormGroup({
      topicName: new FormControl(this.topicName, [
        Validators.required,
        Validators.minLength(4)
      ]),
      topicField: new FormControl(this.topicField, [
        Validators.required,
        Validators.minLength(3)
      ]),
      topicDescription: new FormControl(this.topicDescription, [
        Validators.required
      ])
    });
  }

  get topicName(): any { return this.createForm.get('topicName'); }
  get topicField(): any { return this.createForm.get('topicField'); }
  get topicDescription(): any { return this.createForm.get('topicDescription'); }

  onCreate(): void {
    if (this.createForm?.invalid){
      this.flashMessageService.show('Invalid form', { cssClass: 'alert-popup alert-danger', timeout: 2000 });
    } else {
      this.chatService.createTopic(this.createForm.value).subscribe((data: { success: boolean; msg: string | undefined; }) => {
        if (data.success){
          this.router.navigate(['user/topic']);
          this.flashMessageService.show(data.msg, { cssClass: 'alert-popup alert-success', timeout: 2500 });
        } else {
          this.createForm.reset();
          this.flashMessageService.show(data.msg, { cssClass: 'alert-popup alert-danger', timeout: 3000 });
        }
      });
    }
  }

}
