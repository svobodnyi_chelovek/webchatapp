# Live Web Chat App #

![Preview](./preview/chat.png)

> A small chat application built using the **MEAN** stack  

# Features

- Register and authenticate users using JSON web tokens
- Create a topic
- Join a common topic room where you can chat with all the participants
- Real-time communication
- Have a pleasant UI/UX built to look awesome 

<p align="center">
  <img src="./preview/login.png" alt="Login Preview"/>
</p>
<p align="center">
  <img src="./preview/profile.png" alt="Profile Page Preview"/>
</p>

# Getting started

### Prerequisites

> if you don't know what you are doing go on the website and follow the instructions, those are pretty straight forward

- [NodeJS](https://nodejs.org)
- [MongoDB](https://www.mongodb.com/)
- [Angular-CLI](https://cli.angular.io/)

```bash
# clone the repository
git clone https://svobodnyi_chelovek@bitbucket.org/svobodnyi_chelovek/webchatapp.git

# [backend api]
# change into the repo directory
cd webchatapp

# install server dependencies
npm install

# start the development server
npm run dev


# now open another terminal window
# [frontend angular app]
# change into the angular src directory
cd angular-webchat-ui

# install frontend dependencies
npm install

# start angular development server
npm start or ng serve
```

Then visit http://localhost:4200 in your browser.

# Technologies

- [NodeJS](https://nodejs.org/) - JavaScript backend/server-side solution of choice

- [Express](https://expressjs.com/) - Node framework that makes handling http requests with ease

  - [JsonWebToken](https://www.npmjs.com/package/jsonwebtoken) - package that helps with generating JWTs for secure authentication

  - [PassportJS](http://passportjs.org/) - authentication middleware used to guard certain parts of the app for non-authenticated requests

- [MongoDB](https://www.mongodb.com/) - data storage solution that just speaks JSON and pairs very well with Node

  - [Mongoose](http://mongoosejs.com/) - package that helps with object modeling and manages connection between server and database

  - [Bcryptjs](https://www.npmjs.com/package/bcryptjs) - for salting and hashing the user password to be stored in the database

- [Socket.io](https://socket.io/) - web sockets implementation, fast and reliable real-time communication engine

- [Angular](https://angular.io/) - rich frontend web framework, helps creating fast, reliable web applications

  - [Angular-CLI](https://cli.angular.io/) - command line interface for streamlined angular development

  - [TypeScript](https://www.typescriptlang.org/) - superset of JavaScript that can be compiler-checked, also has types!!

  - [Bootstrap](http://getbootstrap.com/) - CSS/JS framework, makes it easy to develop responsive, well polished web apps

# How it works

On the client-side users can create a new account that will be stored in the database. Then users can authenticate with the given credentials, 
if those are correct the server sends a unique token to the client. The client stores it for use on restricted backend route requests.

Once authenticated, the server creates a socket bidirectional connection with the client to facilitate the chat functionality.

Every time a user sends a message, this goes to the server which broadcast it to the desired users.