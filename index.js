const express = require('express');
const cors = require('cors');
const bodyParser = require("body-parser");
const passport = require('passport');
const logger = require('./log/logger').logger;

//Database requirements
const config = require('./config/config')['development'];
const mongoose = require('mongoose');
const db_url = 'mongodb+srv://' + config.database.username + ':' + config.database.password + '@web-chat-cluster.yd6nd.mongodb.net/web-chat-data?retryWrites=true&w=majority';

//Import routes
const routes = require('./routes/router');

//Initialize the app
const app = express();

// Requirements for implementing bi-directional communication channel between client and server
const http = require('http');
const server = http.createServer(app);
const { Server } = require('socket.io');
const io = new Server(server, {
    cors: {origin: '*'}
});

//Allow requests from another domain / enable Cross-Origin Resource Sharing
app.use(cors());

//Add middlewares
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(passport.initialize());
require('./config/passport')(passport);

//Set routes
app.use(routes);

//Start the server
async function start(){
    try {
        //Get the MongoDB ready
        await mongoose.connect(db_url, {useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true, useUnifiedTopology: true}, err => {
            if (err){
                logger.error(err);
            }else {
                logger.info('Database connection has been established successfully');
            }
        });

        //Set up the server to listen
        server.listen(3000, (err) => {
            if (err) {
                logger.error("Server failed to start");
                process.exit(1);
            } else{
                logger.info('The server started listen at port 3000');
            }
        });
    } catch (e) {
        logger.error(e);
    }
}

start();

const Message = require('./models/message');

// Full duplex communication with server functionality
io.on('connection', socket => {
    logger.info('A user connected');

    socket.on('message', data => {
        socket.broadcast.emit('message', data);

        // Save message to database
        Message.addMessage(new Message(data.msg));
    })
    socket.on('disconnect', () => {
        logger.info('User disconnected');
    })
});