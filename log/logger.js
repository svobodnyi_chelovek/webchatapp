const { createLogger, format, transports, config } = require('winston');
const { combine, timestamp, json } = format;

const logger = createLogger({
    levels: config.syslog.levels,
    format: combine(
        timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        json()
    ),
    transports: [
        new transports.Console(),
        new transports.File({ filename: 'server.log'})
    ]
});

const clientLogger = createLogger({
    defaultMeta: { source: 'Client log '},
    transports: [
        new transports.File({ filename: 'server.log'})
    ]
})
module.exports = {
    logger: logger,
    clientLogger: clientLogger
};