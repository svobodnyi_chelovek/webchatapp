const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const logger = require('../log/logger').logger;
const clientLogger = require('../log/logger').clientLogger;
const User = require('../models/user');
const Topic = require('../models/topic');

//Define API for this app (CRUD operations)

//Get the home page for non-authenticated users
router.get('/', (req, res) => {
    res.redirect('http://localhost:4200');
})

// Create a new user account
router.post('/create',   (req, res) => {
    let response = {success: false};
    const newUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        birthdate: req.body.birthdate
    });
    User.addNewUser(newUser, (err) => {
        if (err) {
            response.msg = err.msg;
            logger.error(`Failed to create a new user: ${err}`);
            res.json(response);
        } else {
            response.success = true;
            response.msg = 'Account created successfully';
            //Send the response
            res.json(response);
        }
    })
})

// User Log in
router.post('/login',  (req, res) => {
    let response = {success: false};
    User.authenticate(req.body.username.trim(), req.body.password.trim(), (err, user) => {
        if (err) {
            response.msg = err.msg;
            res.json(response);
        } else {
            let signData = {
                id: user._id,
                username: user.username,
                email: user.email,
                birthdate: user.birthdate
            };
            // Generate a unique token for the user, wherewith the user can subsequently access parts of the application that need authentication
            // Generate token synchronously
            response.token = jwt.sign(signData, 'privateKey', {expiresIn: 54400});
            response.user = signData;
            response.success = true;
            response.msg = 'User authenticated successfully';
            logger.info(`User ${user.username} authenticated successfully`);
            res.json(response);
        }
    })
})

// Get user list from MongoDB
router.get('/users', passport.authenticate('jwt', {session: false}), (req, res) => {
    User.getUsers().then(users => {
        let response = {
            success: true,
            users: users
        }
        res.json(response);
    }).catch(err => {
        logger.error(`Failed to get users: ${err}`);
    });
})

// Add a topic
router.post('/createTopic/:username', passport.authenticate('jwt', {session: false}), (req, res) => {
    let response = {success: false};
    const newTopic = new Topic({
        name: req.body.topicName,
        field: req.body.topicField,
        description: req.body.topicDescription,
        participants: [],
        owner: req.params.username
    });
    Topic.addTopic(newTopic, (err) => {
        if(err){
            response.msg = "Failed to create a new topic";
            logger.error(`Failed to create a new topic: ${err}`);
        }else {
            response.success = true;
            response.msg = "Topic has been created successfully";
            res.json(response);
        }
    });
})

// Get conversation from MongoDB for a indicated topic
router.get('/conversation/:topicname', passport.authenticate('jwt', {session: false}), (req, res) => {
    Topic.getTopicChatRoom(req.params.topicname, (err, messages) => {
        if (err) {
            logger.error(err);
        } else {
            let response = {
                success: true,
                messages: messages
            }
            return res.json(response);
        }
    });
})

// Get topic list from MongoDB
router.get('/topics', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Topic.getTopics().then(topics => {
        let response = {
            success: true,
            topics: topics
        }
        return res.json(response);
    }).catch(err => {
        logger.error(`Failed to get topics: ${err}`);
    });
})

// Get topic data from MongoDB
router.get('/topic/:topicname', passport.authenticate('jwt', {session: false}), (req, res) => {
    Topic.getTopicByName(req.params.topicname, (err, topic) => {
        if (err){
            logger.error(err);
        }else {
            res.json(topic);
        }
    });
})

// Update topic data -> add participant
router.put('/addParticipant/:topicname', passport.authenticate('jwt', {session: false}), (req, res) => {
    Topic.findOneAndUpdate({name: req.params.topicname}, {$push: {participants: req.body.username}}, (err, result) => {
        if (err){
            logger.error(`Failed to update topic ${req.params.topicname}: ${err}`);
            res.status(500).send(err);
        }else {
            res.json(result);
        }
    })
})

// Update topic data -> exclude participant
router.put('/excludeParticipant/:topicname', passport.authenticate('jwt', {session: false}), (req, res) => {
    Topic.findOneAndUpdate({name: req.params.topicname}, {$pull: {participants: req.body.username}}, (err, result) => {
        if (err) {
            logger.error(`Failed to update topic ${req.params.topicname}: ${err}`);
            res.status(500).send(err);
        } else {
            res.json(result);
        }
    })
})

// Get logs from client-side
router.post('/logs', passport.authenticate('jwt', {session: false}), (req, res) => {
    clientLogger.error(`${req.body.message} : ${req.body.timestamp}`);
})

module.exports = router